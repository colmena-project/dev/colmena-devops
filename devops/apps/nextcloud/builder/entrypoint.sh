#!/bin/bash

set -e

echo ======== Starting Api wrapper colmena ========
echo

sudo a2ensite api.conf

# Run the original entrypoint of the base image
exec /entrypoint.sh "$@"
