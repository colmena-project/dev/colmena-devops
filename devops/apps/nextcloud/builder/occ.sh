#!/bin/bash

create_app_password() {
  set -e

  local nc_username=$1
  local nc_password=$2

  echo "Received an app password creation request"
  export NC_PASS=${nc_password} && php /var/www/html/occ user:add-app-password --password-from-env ${nc_username}
}

case $1 in
  create_app_password)
    # Run the known function
    shift # Remove the first argument (the function name)
    create_app_password "$@" # Assuming create_app_password is a valid function
    exit $?
    ;;
  *)
    # Unknown function, display an error message and exit with an error code
    echo "Error: Unknown occ function '$1'"
    exit 1
    ;;
esac
