import os
import sys

sys.path.insert(0, os.environ.get("APP_PATH"))

from app import app as application

if __name__ == "__main__":
    application.run()
