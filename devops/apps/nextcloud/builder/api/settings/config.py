import os

# Observability
LOGS_DIR = os.environ.get("LOGS_DIR", "logs")

# App password wait tolerance
APP_PASSWORD_CREATION_TIMEOUT = int(os.environ.get("APP_PASSWORD_TIMEOUT", "14"))

# Openapi configuration
OPENAPI_SCHEMA_VERSION = os.environ.get("OPENAPI_SCHEMA_VERSION", "0.1.0")
