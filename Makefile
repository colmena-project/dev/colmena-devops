.PHONY: help start

BINS_DIR = ./bin
ENV_FILE ?= .env
COMPOSE_DIR ?= devops/local/
SHELL := /bin/bash

APPS_DIR ?= devops/apps

NEXTCLOUD_DOCKERFILE_DIR ?= devops/apps/nextcloud/builder
NEXTCLOUD_IMAGE_NAME ?= colmena_nextcloud

NC_DATA_DIR = devops/apps/nextcloud/data

export GREEN=\033[0;32m
export YELLOW=\033[0;33m
export NOFORMAT=\033[0m

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif

default: help

#❓ help: @ Displays this message
help:
	@echo ""
	@echo "List of available MAKE targets for development usage."
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Examples:"
	@echo ""
	@echo "	make ${GREEN}build${NOFORMAT}		- Create all third party application images"
	@echo "	make ${GREEN}devops${NOFORMAT}		- Start all services"
	@echo "	make ${GREEN}setup${NOFORMAT}		- Setup applications"
	@echo ""
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(firstword $(MAKEFILE_LIST))| tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "${GREEN}%-30s${NOFORMAT} %s\n", $$1, $$2}'
	@echo ""

#🗑  clean.nc:@  Cleans up persistent volumes for nextcloud
clean.nc:
	@sudo chown -R `whoami`:`whoami` ${NC_DATA_DIR} && rm -rf ${NC_DATA_DIR} && mkdir -p ${NC_DATA_DIR}

#🐳 docker.build.nc:@ Build a docker image for the nextcloud application
docker.build.nc:
	@docker build \
		--build-arg NEXTCLOUD_VERSION=$(NEXTCLOUD_VERSION) \
		--build-arg NEXTCLOUD_ADMIN_USER=$(NEXTCLOUD_ADMIN_USER) \
		--build-arg NEXTCLOUD_ADMIN_PASSWORD=$(NEXTCLOUD_ADMIN_PASSWORD) \
		-f $(NEXTCLOUD_DOCKERFILE_DIR)/Dockerfile \
		-t $(NEXTCLOUD_IMAGE_NAME) \
		$(APPS_DIR)/nextcloud
		
# @docker image rm colmena_nextcloud || true
# @cd ${COMPOSE_DIR} && docker compose build nextcloud

#🚀 start:@ Start services using Tilt
start:
	@cd ${COMPOSE_DIR} && tilt up

#⚙️  stop:@   Stops the tilt services
stop:
	@cd ${COMPOSE_DIR} && docker compose down

#💻 connect.dev: @ Start dev a tmux session
connect.dev: SESSION:=dev
connect.dev: DEV_USER:=${DEV_USER}
connect.dev: DEV_HOST:=${DEV_HOST}
connect.dev:
	@${BINS_DIR}/tmux.sh ${SESSION}
