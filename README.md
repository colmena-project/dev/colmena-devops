# Colmena Devops

This repository provides all developer operations setup related to third party applications for the Colmena environment. The idea is to centralize and develop an efficient and zero-effort (or almost zero) setup configuration for developers and devops engineers.

Changes to this repository should consider scaling up devops and environment configurations from local machines to remote machines. For example: we might want to run CI triggers from this repository to easily deploy a nextcloud, ldap or any other application. Templates, charts, compose or whatever file we need for our deployment strategy should be versioned under the devops directory, probably with a subfolder describing the environment name (e.g.: `dev`).

## Requirements

> Note that this has been defined to use the latest versions but might work with not so old versions too. Take this versions as the upmost and desirable requirement.

* Docker: [`26.0.2`](https://docs.docker.com/desktop/install/ubuntu/)
* Compose plugin: [`v2.27.0`](https://docs.docker.com/compose/install/linux/)
* Tilt: [`0.33.14`](https://docs.tilt.dev/#macoslinux)

## Getting started

This section follow the steps to properly configure the colmena devops project and their apps in your local environment:

1. Complete the __Environment variables setup__
2. Run Tilt in the __Local environmemt__ section.
3. Setup nextcloud in the __Additional setup steps__ section.

### Environment variables

Make a copy of the base env file

```bash
cp .env.example .env
```

Values marked with a default do not necessarily need to be changed, meaning values from the _.env.example_ file are valid.

__Postgres:__

* `POSTGRES_HOSTNAME`: the postgres hostname (default: `postgres`)
* `POSTGRES_USERNAME`: the postgres username (default: `postgres`)
* `POSTGRES_PASSWORD`: the postgres password (default: `postgres`)

__PGAdmin:__

* `PGADMIN_DEFAULT_EMAIL`: the postgres admin user email (default: `admin@colmena.media`)
* `PGADMIN_DEFAULT_PASSWORD`: the postgres admin user password (default: `admin`)

__Nextcloud:__

* `NEXTCLOUD_VERSION`: The nextcloud version (default: `25.0.3`)
* `NEXTCLOUD_HOSTNAME`: The nextcloud hostname (default: `nextcloud`)
* `NEXTCLOUD_ADMIN_USER`: The initial nextcloud admin user (default: `superadmin`). This user is created when the app builds for the first time, using the password provided in the variable below.
* `NEXTCLOUD_ADMIN_PASSWORD`: The initial nextcloud admin password (default: `superadmin`)
* `NEXTCLOUD_TRUSTED_DOMAINS`: The nextcloud trusted domains (default: `"*"`, accepts request from any domain)
* `NEXTCLOUD_UPDATE`: Whether the nextcloud build process accepts other commands to be run at build time. (default: `1`). This should be enabled to run custom applications in the same container, such as the nextcloud api wrapper.
* `NEXTCLOUD_API_WRAPPER_PORT`: The port assigned to the nextcloud api wrapper (default: `5001`)
* `APACHE_APP_PATH`: The directory where the api wrapper installation will be located. (default: `/var/www/nc_api_wrapper`)

__Remote environments sessions:__

> These are shortcuts to access the remote environment

* `DEV_USER`: The ssh user to access a remote virtual machine.
* `DEV_HOST`: The host of a virtual machine.

### Run the local environmemt

We use `make` to quickly run predefined tasks to setup, start and stop our environment.

```bash
# Type make to display the default help message.
# You'll notice a bunch of suggestions, skip them for now, they'll be useful as
# a reminder.
make
```

Now you can start all services:

```bash
make start
```

This will prompt a few options in your terminal, press T to navgate through services or Spacebar to open a friendly user interface. There you'll be able to get every app status and search for logs. If you did not install `tilt` then type `make devops.up`, it will start services with `docker compose`. You can then put them down with `make devops.down`.

That's all. The next time you need to start services just run:

```bash
make start
```

To stop services run:

```bash
make stop
```

### Admin & Management applications

#### Postgres Admin

The local development environment uses a postgres database. We use the PGAdmin app to debug registries, create queries or just inspect information from the database.

See instructions at the [apps/pgadmin](./devops/apps/pgadmin/README.md) section.
